package test.gitlab.service;

import org.junit.Test;

import static org.junit.Assert.*;

public class MyServiceTest {

    private MyService myService = new MyService();

    @Test
    public void sum() {

        // arrange
        int a = 3, b = 2;

        // act
        int sum = myService.sum(a, b);

        // assert
        assertEquals("Wrong sum", 5, sum);
    }
}